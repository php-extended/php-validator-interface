<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

use Stringable;

/**
 * ArrayValidatorInterface interface file.
 * 
 * This validator validates an array of similar attribute values.
 * 
 * @author Anastaszor
 */
interface ArrayValidatorInterface extends Stringable
{
	
	/**
	 * Validates an array of similar elements. for each element, a specific
	 * attribute name is issued and each element is validated one after another.
	 * If there is no error nor transforms an empty array is returned.
	 * 
	 * @param string $attrName
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $attrValues
	 * @return array<integer, ValidationResultInterface>
	 */
	public function validateArray(string $attrName, array $attrValues) : array;
	
}
