<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

use Stringable;

/**
 * ValidationResultInterface interface file.
 * 
 * This interface represents an error of validation that is specific to a
 * single attribute.
 * 
 * @author Anastaszor
 */
interface ValidationResultInterface extends Stringable
{
	
	/**
	 * Gets whether this validation result is an error or a transform.
	 * 
	 * @return boolean
	 */
	public function isError() : bool;
	
	/**
	 * Gets the name of the attribute that has the value that was not validated
	 * nor could not be transformed.
	 * 
	 * @return string
	 */
	public function getAttributeName() : string;
	
	/**
	 * Gets the value that was not validated, or that was transformed.
	 * 
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function getTransformedValue();
	
	/**
	 * Gets the error message that can help validate the value, in case it 
	 * could not be transformed. If it was transformed, gives a message that
	 * describe the transformation.
	 * 
	 * @return string
	 */
	public function getMessage() : string;
	
}
