<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

use Stringable;

/**
 * ObjectValidatorInterface interface file.
 * 
 * This validator validates the attributes of an object with the complex rules
 * that may be associated to a single attribute for that object.
 * 
 * @author Anastaszor
 */
interface ObjectValidatorInterface extends Stringable
{
	
	/**
	 * Validates the given object with the inner rules of the validator, and
	 * returns an array of validation errors. If there are no errors nor
	 * transforms, an empty array is returned.
	 * 
	 * @param object $object
	 * @return array<integer, ValidationResultInterface>
	 */
	public function validate($object) : array;
	
}
