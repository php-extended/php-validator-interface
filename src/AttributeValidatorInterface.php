<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

use Stringable;

/**
 * AttributeValidatorInterface interface file.
 * 
 * This validator validates a single attribute value.
 * 
 * @author Anastaszor
 */
interface AttributeValidatorInterface extends Stringable
{
	
	/**
	 * Validates the given attribute value with the given attribute name and
	 * gives back a list of validation errors. If there is no errors nor
	 * transforms, then the array should be empty.
	 * 
	 * @param string $attrName
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $attrValue
	 * @return array<integer, ValidationResultInterface>
	 */
	public function validate(string $attrName, $attrValue) : array;
	
}
